Welcome to pytidyclub!
^^^^^^^^^^^^^^^^^^^^^^
pytidyclub is a simple Python wrapper for the TidyClub API.

.. image:: https://travis-ci.org/KyeRussell/pytidyclub.svg?branch=master
    :target: https://travis-ci.org/KyeRussell/pytidyclub

.. image:: https://coveralls.io/repos/KyeRussell/pytidyclub/badge.svg
  :target: https://coveralls.io/r/KyeRussell/pytidyclub


TidyClub_ is a powerful online tool that removes a lot of hassle from the management of your
club or association. Unfortunately, TidyClub doesn't do everything, but they provide an API_
which you can use to extend it. pytidyclub is a Python wrapper for this API, which makes
creating Python projects that interface with the TidyClub API easier.

pytidyclub is **not** a complete implementation of the TidyClub _API.

Installing
^^^^^^^^^^
You can install pytidyclub with ``pip``::

    pip install pytidyclub
    
Example
^^^^^^^

::

    from pytidyclub import pytidyclub
    
    club = pytidyclub.Club(
        slug = default,
        client_id = "12345J",
        client_secret = "54321F"
    )
    
    # Authenticate as a user.
    print club.auth_authcode_get_url("urn:ietf:wg:oauth:2.0:oob")
    club.auth_authcode_exchange_code(raw_input("Click the link above and approve - paste auth code here: "), "urn:ietf:wg:oauth:2.0:oob")

    # Get all contacts.
    contacts = club.contacts()
    
    # Search for contacts
    contacts = club.contacts("Kye Russell")


.. _TidyClub: http://tidyclub.com
.. _API: http://dev.tidyclub.com